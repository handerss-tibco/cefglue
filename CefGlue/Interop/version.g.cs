﻿//
// DO NOT MODIFY! THIS IS AUTOGENERATED FILE!
//
namespace Xilium.CefGlue.Interop
{
    using System;
    using System.Runtime.InteropServices;
    using System.Diagnostics.CodeAnalysis;
    
    internal static unsafe partial class libcef
    {
        public const string CEF_VERSION = "107.0.3+ge92a530+chromium-107.0.5304.18";
        public const int CEF_VERSION_MAJOR = 107;
        public const int CEF_COMMIT_NUMBER = 2672;
        public const string CEF_COMMIT_HASH = "e92a530af3bf8966524e630cb13acce1d3306dac";
        
        public const int CHROME_VERSION_MAJOR = 107;
        public const int CHROME_VERSION_MINOR = 0;
        public const int CHROME_VERSION_BUILD = 5304;
        public const int CHROME_VERSION_PATCH = 18;
        
        public const string CEF_API_HASH_UNIVERSAL = "44197292401010f8fce5b053733edd8642d01095";
        
        public const string CEF_API_HASH_PLATFORM_WIN = "95bf7fa1356070be95b7a6fee958355c6619fb63";
        public const string CEF_API_HASH_PLATFORM_MACOS = "8ec5426d7aa0418fca147380e97623a49cd8eaf4";
        public const string CEF_API_HASH_PLATFORM_LINUX = "b2cbc2e6a3048d2415566d35ba434967fd796491";
    }
}
